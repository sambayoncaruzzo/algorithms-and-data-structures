// Package algorithms ...
package algorithms

// InsertSort ...
/*
BigO = n^2
worst-case running time as an2+ bn+ c
*/
func InsertSort(list []int) []int {
	length := len(list)

	for i := 1; i < length; i++ {
		key := list[i]
		prevIndex := i - 1

		for ; prevIndex >= 0 && list[prevIndex] > key; prevIndex-- {
			list[prevIndex+1] = list[prevIndex]
		}
		list[prevIndex+1] = key
	}
	return list
}

// ReverseInsertSort ...
/*
BigO = n^2
worst-case running time as an2+ bn+ c
*/
func ReverseInsertSort(list []int) []int {
	length := len(list)

	for i := 1; i < length; i++ {
		key := list[i]
		prevIndex := i - 1
		for ; prevIndex >= 0 && list[prevIndex] < key; prevIndex-- {
			list[prevIndex+1] = list[prevIndex]
		}
		list[prevIndex+1] = key
	}
	return list
}

// SelectionSort ...
func SelectionSort(list []int) []int {
	length := len(list)

	for i := 0; i < length; i++ {
		minIndex := i
		for s := i + 1; s < length; s++ {
			if list[minIndex] > list[s] {
				minIndex = s
			}
		}
		list[i], list[minIndex] = list[minIndex], list[i]
	}
	return list
}

// MergeSort ...
func MergeSort(list []int) []int {
	length := len(list)
	if length < 2 {
		return list
	}
	middle := length / 2
	left := list[:middle]
	right := list[middle:]
	return merge(MergeSort(left), MergeSort(right))
}

func merge(left []int, right []int) []int {
	lengthLeft := len(left)
	lengthRight := len(right)
	lengthTotal := lengthLeft + lengthRight
	orderedList := make([]int, lengthTotal)
	l := 0
	r := 0

	for i := 0; i < lengthTotal; i++ {
		if l > lengthLeft-1 && r <= lengthRight-1 {
			orderedList[i] = right[r]
			r++
		} else if r > lengthRight-1 && l <= lengthLeft-1 {
			orderedList[i] = left[l]
			l++
		} else if left[l] < right[r] {
			orderedList[i] = left[l]
			l++
		} else {
			orderedList[i] = right[r]
			r++
		}
	}
	return orderedList
}

// BubbleSort ...
func BubbleSort(list []int) []int {
	unsortedIndex := len(list) - 1
	sorted := false
	for !sorted {
		sorted = true
		for i := 0; i < unsortedIndex; i++ {
			if list[i] > list[i+1] {
				sorted = false
				list[i], list[i+1] = list[i+1], list[i]
			}
		}
		unsortedIndex = unsortedIndex - 1
	}
	return list
}
