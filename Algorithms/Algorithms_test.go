package algorithms

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
)

func Test_Insertion_Sort(t *testing.T) {
	testList := []int{5, 2, 4, 6, 1, 3}
	expected := []int{1, 2, 3, 4, 5, 6}
	list := InsertSort(testList)
	stringList, _ := fmt.Printf("result: %v ", list)
	assert.True(t, cmp.Equal(expected, list), stringList)
}

func Test_Reverse_Insertion_Sort(t *testing.T) {
	testList := []int{5, 2, 4, 6, 1, 3}
	expected := []int{6, 5, 4, 3, 2, 1}
	list := ReverseInsertSort(testList)
	stringList, _ := fmt.Printf("result: %v ", list)
	assert.True(t, cmp.Equal(expected, list), stringList)
}

func Test_Selection_Sort(t *testing.T) {
	testList := []int{5, 2, 4, 6, 1, 3}
	expected := []int{1, 2, 3, 4, 5, 6}
	list := SelectionSort(testList)
	stringList, _ := fmt.Printf("result: %v ", list)
	assert.True(t, cmp.Equal(expected, list), stringList)
}

func Test_Merge_Sort(t *testing.T) {
	testList := []int{5, 2, 4, 6, 1, 3}
	expected := []int{1, 2, 3, 4, 5, 6}
	list := MergeSort(testList)
	fmt.Printf("result: %v, expected: %v ", list, expected)
	assert.True(t, cmp.Equal(expected, list), "List not in Order")
}

func Test_Bubble_Sort(t *testing.T) {
	testList := []int{5, 2, 4, 6, 1, 3}
	expected := []int{1, 2, 3, 4, 5, 6}
	list := BubbleSort(testList)
	fmt.Printf("result: %v, expected: %v ", list, expected)
	assert.True(t, cmp.Equal(expected, list), "List not in Order")
}
